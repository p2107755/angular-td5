import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { VolComponent } from '../vol/vol.component';
import { CommonModule } from '@angular/common';
import { MatGridTile } from '@angular/material/grid-list';
import { MatGridList } from '@angular/material/grid-list';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [CommonModule, VolComponent, MatGridTile, MatGridList],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input() vols!: Vol[];
  @Input() type!: String;
  @Output() clickVol = new EventEmitter<Vol>();


  choixVol(vol: Vol){
    this.clickVol.emit(vol);
  }

}
