import { Component, LOCALE_ID, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { AEROPORTS } from './../../constants/aeroport.constant';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { IAeroport } from '../../models/aeroport.model';
import { ThreeDayRangeSelectionStrategy } from '../../date-adapter';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import {MatCommonModule} from '@angular/material/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { IFiltres } from '../../models/filtres.model';


@Component({
  selector: 'app-filtres',
  standalone: true,
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  imports: [MatIconModule, MatButtonModule, MatInputModule,
    MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatCommonModule, CommonModule, ReactiveFormsModule],
  providers: [
    provideNativeDateAdapter(),
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: ThreeDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  @Output() filtresEvent = new EventEmitter<IFiltres>();

  aeroports: IAeroport[] = AEROPORTS;

  aeroport = new FormControl(null, Validators.required);
  debut = new FormControl(null, Validators.required);
  fin = new FormControl(null, Validators.required);

  filtres(): void {
    this.filtresEvent.emit({
      aeroport: this.aeroport.value!,
      debut: this.debut.value!,
      fin: this.fin.value!
    });
  }

  checkFiltres(): boolean {
    return !!this.aeroport?.errors?.['required'] || !!this.debut?.errors?.['required'] || !!this.fin?.errors?.['required'];
  }
  
}
