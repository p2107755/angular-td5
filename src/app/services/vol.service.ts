import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { COMPAGNIES } from './../constants/compagnie.constant';
import { IVolDto, Vol } from './../models/vol.model';

@Injectable({
  providedIn: 'root'
})
export class VolService {

  constructor(private http: HttpClient) { }
  getVolsDepart(code: string, debut: number, fin: number): Observable<Vol[]> {
    return this.http.get<any>(`https://opensky-network.org/api/flights/departure?airport=${code}&begin=${debut}&end=${fin}`).pipe(
      map((response) => response
        .filter((dto: IVolDto) => this.isAirFrance(dto))
        .map((dto: IVolDto) => new Vol(dto))
    ));
  }

  getVolsArrivee(code: string, debut: number, fin: number): Observable<Vol[]> {
    return this.http.get<any>(`https://opensky-network.org/api/flights/arrival?airport=${code}&begin=${debut}&end=${fin}`).pipe(
      map((response) => response
        .filter((dto: IVolDto) => this.isAirFrance(dto))
        .map((dto: IVolDto) => new Vol(dto))
    ));
  }

  private isAirFrance(dto: IVolDto): boolean {
    return !!Object.keys(COMPAGNIES).find((key$) =>
      dto.callsign.includes(key$)
    );
  }
}
