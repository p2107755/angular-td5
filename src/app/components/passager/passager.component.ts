import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatGridTile } from '@angular/material/grid-list';
import { MatGridList } from '@angular/material/grid-list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DirectivesModule } from '../directive/directive-modules';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [CommonModule, MatIconModule, MatBadgeModule, MatGridTile, MatGridList, MatTooltipModule, DirectivesModule],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: Passager;
  @Input() photos!: boolean;
}
