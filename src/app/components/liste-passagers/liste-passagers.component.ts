import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { PassagerComponent } from '../passager/passager.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatGridTile } from '@angular/material/grid-list';
import { MatGridList } from '@angular/material/grid-list';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [CommonModule, PassagerComponent, MatSlideToggleModule, MatGridTile, MatGridList, FormsModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers!: Passager[];
  checked = false;

}
