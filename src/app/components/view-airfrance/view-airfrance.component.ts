import { Component, OnDestroy, OnInit } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IFiltres } from '../../models/filtres.model';
import { Subscription, from } from 'rxjs';
import { Vol } from '../../models/vol.model';
import { VolService } from '../../services/vol.service';
import { CommonModule, UpperCasePipe } from '@angular/common';  
import { ActivatedRoute } from '@angular/router';
import { Passager } from '../../models/passager.model';
import { PassagerService } from '../../services/passager.service';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [CommonModule, ListeVolsComponent, FiltresComponent, ListePassagersComponent, UpperCasePipe],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit, OnDestroy {

  vols: Vol[] = [];
  private subscription: Subscription = new Subscription();
  selectedVol!: Vol;
  passagers!: Passager[];
  type!: string;

  constructor(
    private _volService: VolService,
    private _activatedRoute: ActivatedRoute,
    private _passagerService: PassagerService) {}

  ngOnInit(): void {
    const subscription = this._activatedRoute.data.subscribe((data$) => {
    this.type = data$['type'] ? data$['type'] : 'decollages';
    })
    
    this.subscription.add(subscription);
  }
  

  onFiltresEvent(filtres: IFiltres): void{
    let { aeroport, debut, fin} = filtres;

    let debutSecondes = Math.floor(debut.getTime()/1000);
    let finSecondes = Math.floor(fin.getTime()/1000);
    let icao = aeroport.icao;

    let subscription: Subscription;

    if (this.type == "decollage"){
      subscription = this._volService.getVolsDepart(icao, debutSecondes, finSecondes).subscribe((value) => {
        this.vols = value;
      })
    } else {
      subscription = this._volService.getVolsArrivee(icao, debutSecondes, finSecondes).subscribe((value) => {
        this.vols = value;
      })
    }

    this.subscription.add(subscription);
  }

  setSelectedVol(vol: Vol){
    this.selectedVol = vol;

    const subscription: Subscription = this._passagerService.getPassagers(vol.icao).subscribe((value) => {
      this.passagers = value;
    });
    this.subscription.add(subscription);

  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }
}
