import { Component, Input } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIcon } from '@angular/material/icon';
import { MatGridTile } from '@angular/material/grid-list';


@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [CommonModule, MatGridListModule, MatIcon, MatGridTile],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() vol!: Vol;
  @Input() type!: String;

}
