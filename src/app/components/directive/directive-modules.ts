import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClasseBagagesDirective } from './classe-bagages.directive';
import { ClasseVolDirective } from './classe-vol.directive';

@NgModule({
  declarations: [ClasseBagagesDirective, ClasseVolDirective],
  exports: [ClasseBagagesDirective, ClasseVolDirective],
  imports: [CommonModule]
})
export class DirectivesModule {}