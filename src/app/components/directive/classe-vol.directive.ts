import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ClasseVol } from '../../models/passager.model';

@Directive({
  selector: '[appClasseVol]'
})
export class ClasseVolDirective implements OnChanges {

  @Input() appClasseVol!: String;

  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.appClasseVol == 'BUSINESS') {
      this.el.nativeElement.style.color = 'red';
    }

    if (this.appClasseVol == 'STANDARD') {
      this.el.nativeElement.style.color = 'blue';
    }

    if (this.appClasseVol == 'PREMIUM') {
      this.el.nativeElement.style.color = 'green';
    }
  }

}
