import { Directive, ElementRef, Input, NgModule, OnChanges, SimpleChanges } from '@angular/core';
import { BagageClasse } from '../../models/bagageClasse.model';

@Directive({
  selector: '[bagagesClasse]'
})

export class ClasseBagagesDirective implements OnChanges {

  @Input() bagagesClasse!: BagageClasse;

  constructor(private el: ElementRef) { }



  ngOnChanges(changes: SimpleChanges): void {
    let { classeVol, nbBagSoute } = this.bagagesClasse;

    switch (classeVol) {
      case 'STANDARD':
        if (nbBagSoute > 1) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
      case 'BUSINESS':
        if (nbBagSoute > 2) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
      case 'PREMIUM':
        if (nbBagSoute > 3) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
    }
  }

}
